using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using System.Xml.Linq;



namespace WebDriver_Task3
{
    [TestFixture]
    public class TestsGoogle
    {
        private ChromeDriver _driver;        

        [SetUp]
        public void Setup()
        {
            _driver = new ChromeDriver();
            _driver.Manage().Window.Maximize();
        }

        [Test]
        public void TestGoogleCalculator()
        {
            GoogleCloudPage googleCloud = new(_driver);
            Assertions assertions = new (_driver);
            googleCloud.OpenPage();
            googleCloud.SearchGoogleCloudPlatformPricingCalculator();
            googleCloud.AddToEstimate();
            googleCloud.AddNumberOfInstance();
            googleCloud.ChoiceOperationSystem();
            googleCloud.ChoiceProvisioningModel();
            googleCloud.ChoiceMachineFamily();
            googleCloud.ChoiceSeries();
            googleCloud.ChoiceMachineType();
            googleCloud.TurnAddGPUButton();
            googleCloud.ChoiceGPUModel();
            googleCloud.ChoiceNumberOfGPUs();
            googleCloud.ChoiceLocalSSD();
            googleCloud.ChoiceRegion();
            googleCloud.ChoiceCommitedUsage();
            googleCloud.CountTotalEstimate();
            googleCloud.ShareEstimate();
            googleCloud.OpenEstimateSummary();
            googleCloud.SwitchBrowserWindow(1);
            assertions.DoAssert();
        }

        [TearDown]
        public void Teardown() 
        {
            _driver.Close();
            _driver.Quit();
        }
    }
}