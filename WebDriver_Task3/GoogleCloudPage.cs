﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.Extensions;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.PageObjects;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace WebDriver_Task3
{
    public class GoogleCloudPage
    {
        private const string _BASE_URL = "https://cloud.google.com/";
        private const string _inputMachineFamily = "general-purpose";
        private const string _inputComputeEngineValue = "COMPUTE ENGINE";
        private const string _inputInstancesNumber = "4";
        private const string _inputTotalPrice = "$5,628.90";
        private const string _inputOperationSystem = "free-debian-centos-coreos-ubuntu";
        private const string _inputSeriesValue = "n1";
        private const string _inputMachineTypeValue = "n1-standard-8";
        private const string _inputGPUModelField = "GPU Model";
        private const string _inputGPUModelValue = "nvidia-tesla-v100";
        private const string _inputNumberOfGPUField = "Number of GPUs";
        private const string _inputLocalSSDValue = "2x375 GB";
        private const string _inputRegionValue = "europe-west4";
        private const string _inputNumberOfGPUValue = "1";
        private const string _inputUsageTime = "1 year";
        private const string _inputCurrencySymbol = "USD";
        private const string _inputExpectedPerTime = "/ month";

        [FindsBy(How = How.ClassName, Using = "YSM5S")]
        private readonly IWebElement? _searchIcon;

        [FindsBy(How = How.ClassName, Using = "qdOxv-fmcmS-wGMbrd")]
        private readonly IWebElement? _inputSearchField;

        [FindsBy(How = How.CssSelector, Using = ".PETVs.PETVs-OWXEXe-UbuQg")]
        private readonly IWebElement? _pressSearchButton;

        [FindsBy(How = How.ClassName, Using = "K5hUy")]
        private readonly IWebElement? _searchResultElement;

        [FindsBy(How = How.ClassName, Using = "UywwFc-vQzf8d")]
        private readonly IWebElement? _addToEstimate;

        [FindsBy(How = How.Id, Using = "c2")]
        private readonly IWebElement? _searchComputeEngine;

        [FindsBy(How = How.ClassName, Using = "BPgnDc")]
        private readonly IWebElement? _computeEngineButton;

        [FindsBy(How = How.Id, Using = "c118")]
        private readonly IWebElement? _nameOfServiceType;

        [FindsBy(How = How.XPath, Using = "//li[@data-value='9']")]
        private readonly IWebElement? _serviceType;

        [FindsBy(How = How.Id, Using = "c11")]
        private readonly IWebElement? _numberOfInstance;

        [FindsBy(How = How.Id, Using = "c19")]
        private readonly IWebElement? _operationSystem;

        [FindsBy(How = How.XPath, Using = $"//li[contains(@data-value, '{_inputOperationSystem}')]")]
        private readonly IWebElement? _operationSystemValue;

        [FindsBy(How = How.Id, Using = "regular")]
        private readonly IWebElement? _provisioningModel;

        [FindsBy(How = How.Id, Using = "c26")]
        private readonly IWebElement? _machineFamily;

        [FindsBy(How = How.XPath, Using = $"//li[contains(@data-value, '{_inputMachineFamily}')]")]
        private readonly IWebElement? _machineFamilyValue;

        [FindsBy(How = How.Id, Using = "c30")]
        private readonly IWebElement? _series;

        [FindsBy(How = How.XPath, Using = $"//li[contains(@data-value, '{_inputSeriesValue}')]")]
        private readonly IWebElement? _seriesValue;

        [FindsBy(How = How.Id, Using = "c33")]
        private readonly IWebElement? _machineType;

        [FindsBy(How = How.XPath, Using = $"//li[contains(@data-value, '{_inputMachineTypeValue}')]")]
        private readonly IWebElement? _machineTypeValue;

        [FindsBy(How = How.CssSelector, Using = "div.AsBIyb button[aria-label='Add GPUs']")]
        private readonly IWebElement? _addGPUButton;

        [FindsBy(How = How.XPath, Using = $"//span[text()='{_inputGPUModelField}']")]
        private readonly IWebElement? _gPUModel;

        [FindsBy(How = How.XPath, Using = $"//li[contains(@data-value, '{_inputGPUModelValue}')]")]
        private readonly IWebElement? _gPUModelValue;

        [FindsBy(How = How.XPath, Using = $"//span[text()='{_inputNumberOfGPUField}']")]
        private readonly IWebElement? _numberOfGPUs;

        [FindsBy(How = How.XPath, Using = $"//li[@data-value='{_inputNumberOfGPUValue}']")]
        private readonly IWebElement? _numberOfGPUsValue;

        [FindsBy(How = How.Id, Using = "c42")]
        private readonly IWebElement? _localSSD;

        [FindsBy(How = How.XPath, Using = $"//li[contains(., '{_inputLocalSSDValue}')]")]
        private readonly IWebElement? _localSSDValue;

        [FindsBy(How = How.Id, Using = "c46")]
        private readonly IWebElement? _region;

        [FindsBy(How = How.XPath, Using = $"//li[@data-value='{_inputRegionValue}']")]
        private readonly IWebElement? _regionValue;

        [FindsBy(How = How.XPath, Using = $"//label[contains(text(), '{_inputUsageTime}')]")]
        private readonly IWebElement? _commitedUsage;

        [FindsBy(How = How.XPath, Using = "//span[text()='Add to estimate']")]
        private readonly IWebElement? _addToEstimateButton;

        [FindsBy(How = How.XPath, Using = "//button[@data-idom-class='bwApif-zMU9ub']")]
        private readonly IWebElement? _closeEstimateButton;

        [FindsBy(How = How.ClassName, Using = "FOBRw-vQzf8d")]
        private readonly IWebElement? _shareButton;

        [FindsBy(How = How.CssSelector, Using = "[track-name='open estimate summary']")]
        private readonly IWebElement? _openEstimateSummaryButton;

        [FindsBy(How = How.XPath, Using = "//span[@class='MyvX5d D0aEmf']")]
        private readonly IWebElement? _amount;

        [FindsBy(How = How.CssSelector, Using = "span.AeBiU-vQzf8d")]
        private readonly IWebElement? _existCurrencyElement;

        [FindsBy(How = How.ClassName, Using = "WUm90")]
        private readonly IWebElement? _existPerTime;


        private readonly IWebDriver _driver;

        public GoogleCloudPage(IWebDriver driver)
        {
            this._driver = driver;
            PageFactory.InitElements(this._driver, this);
        }

        public void OpenPage()
        {
            _driver.Navigate().GoToUrl(_BASE_URL);
        }

        public void SearchGoogleCloudPlatformPricingCalculator()
        {
            _searchIcon?.Click();
            _inputSearchField?.SendKeys("Google Cloud Platform Pricing Calculator");
            _pressSearchButton?.Click();
            _inputSearchField?.Click();
            WaitExistByClassName("K5hUy");
            _searchResultElement?.Click();
        }

        public void AddToEstimate()
        {
            _addToEstimate?.Click();
            WaitVisibleById("c2");
            _searchComputeEngine?.SendKeys(_inputComputeEngineValue);
            _computeEngineButton?.Click();
            WaitVisibleById("c118");
            ClickJS(_nameOfServiceType);
            _serviceType?.Click();
        }

        public void AddNumberOfInstance()
        {
            _numberOfInstance?.Clear();
            _numberOfInstance?.SendKeys($"{_inputInstancesNumber}");
        }

        public void ChoiceOperationSystem()
        {
            ClickJS(_operationSystem);
            _operationSystemValue?.Click();
        }
        public void ChoiceProvisioningModel()
        {
            ScroolJS(_provisioningModel);
            ClickJS(_provisioningModel);
        }
        public void ChoiceMachineFamily()
        {
            ClickJS(_machineFamily);
            _machineFamilyValue?.Click();
        }
        public void ChoiceSeries()
        {
            ClickJS(_series);
            _seriesValue?.Click();
        }
        public void ChoiceMachineType()
        {
            ClickJS(_machineType);
            _machineTypeValue?.Click();
        }
        public void TurnAddGPUButton()
        {
            _addGPUButton?.Click();
            Assert.That(_addGPUButton?.GetAttribute("aria-checked"), Is.EqualTo("true"), "Switch is not turned on.");
        }
        public void ChoiceGPUModel()
        {
            WaitVisibleByXPath("//span[text()='GPU Model']");
            ClickJS(_gPUModel);
            _gPUModelValue?.Click();
        }
        public void ChoiceNumberOfGPUs()
        {
            ClickJS(_numberOfGPUs);
            _numberOfGPUsValue?.Click();
        }
        public void ChoiceLocalSSD()
        {
            ClickJS(_localSSD);
            _localSSDValue?.Click();
        }
        public void ChoiceRegion()
        {
            ClickJS(_region);
            _regionValue?.Click();
        }
        public void ChoiceCommitedUsage()
        {
            _commitedUsage?.Click();
            Assert.That(_existCurrencyElement?.Text, Is.EqualTo(_inputCurrencySymbol), "Currency is not USD");
        }
        public void CountTotalEstimate()
        {
            _addToEstimateButton?.Click();
            _closeEstimateButton?.Click();
            WaitTextPresent(_amount, _inputTotalPrice);
            Assert.That(_amount?.Text, Is.EqualTo(_inputTotalPrice), "Price not equal $5,628.90");
            Assert.That(_existPerTime?.Text, Is.EqualTo(_inputExpectedPerTime), "Price are not for per month");
        }
        public void ShareEstimate()
        {
            ClickJS(_shareButton);
        }
        public void OpenEstimateSummary()
        {
            ClickJS(_openEstimateSummaryButton);
        }
        public void WaitVisibleByXPath(string path)
        {
            WebDriverWait wait = new (_driver, TimeSpan.FromSeconds(10));
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(path)));
        }
        public void WaitVisibleById(string id)
        {
            WebDriverWait wait = new (_driver, TimeSpan.FromSeconds(10));
            wait.Until(ExpectedConditions.ElementIsVisible(By.Id(id)));
        }
        public void WaitExistByClassName(string path)
        {
            WebDriverWait wait = new (_driver, TimeSpan.FromSeconds(10));
            wait.Until(ExpectedConditions.ElementExists(By.ClassName(path)));
        }
        public void WaitTextPresent(IWebElement? element, string expectedText)
        {
            WebDriverWait wait = new (_driver, TimeSpan.FromSeconds(10));
            wait.Until(ExpectedConditions.TextToBePresentInElement(element, expectedText));
        }
        public void ClickJS(IWebElement? element)
        {
            _driver.ExecuteJavaScript("arguments[0].click();", element);
        }
        public void ScroolJS(IWebElement? element)
        {
            _driver.ExecuteJavaScript("arguments[0].scrollIntoView(true);", element);
        }

        public void Click(IWebElement element)
        {
            element.Click();
        }

        public void SendKeys(IWebElement element, string inputText)
        {
            element.SendKeys(inputText);
        }
        public void SwitchBrowserWindow(int tabNumber)
        {
            _driver.SwitchTo().Window(_driver.WindowHandles[tabNumber]);
        }        
    }
}
